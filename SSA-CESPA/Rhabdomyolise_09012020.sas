/*Reperage des Rhabdomyolyse d'effort en 2016*/

/*M62890: Rhabdomyolyse
  T670: Coup de chaleur et insolation
  T733: Epuisement d  un effort physique intense*/

/* Fusion des tables MCO_B et MCO_D pour avoir les diagnostics principaux, relis et associs*/
proc sql;
	create table mco16b as
	select t1.eta_num, t1.rsa_num,t1.GRG_GHM,
	       t1.SEJ_NBJ, t1.dgn_pal, t1.DGN_REL, t2.ass_dgn
	from oravue.t_mco16b as t1 left join oravue.t_mco16d as t2
	on (t1.eta_num = t2.eta_num and t1.rsa_num = t2.rsa_num)
	where t1.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297','600100101', '750041543', 
                             '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', 
                             '750100166', '750100208', '750100216', '750100232', '750100273', '750100299' , '750801441', 
                             '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', '920100039', 
                             '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', '940100027', 
                             '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137', 
                             '690784152', '690784178', '690787478', '830100558') 
    and t1.GRG_GHM not like '90%' and ((t1.sej_typ not = 'B' or t1.sej_typ is missing) or (t1.ent_mod<>"0" and t1.sor_mod<>"0"))
	and (t2.ass_dgn in ("M62890","T733","T670") or t1.dgn_pal in ("M62890","T733","T670") or t1.dgn_rel in ("M62890","T733","T670"))
order by t1.eta_num, t1.rsa_num;
quit;

/* on deplace la table dans orauser */
%_eg_conditional_dropds(orauser.mco16b);
data orauser.mco16b;
set mco16b;
run;

/* Fusion avec la table MCO_C pour rcuprer le nir et les dates du sjour*/
proc sql;
	create table mco16c as
	select t2.NIR_ANO_17, t2.exe_soi_dtd, t2.EXE_SOI_DTF,t1.* 
	from orauser.mco16b as t1 inner join oravue.t_mco16c as t2
	on (t1.eta_num = t2.eta_num and t1.rsa_num = t2.rsa_num)
	where (t2.NIR_RET="0" 
		and t2.NAI_RET ="0" and t2.SEX_RET ="0" and t2.SEJ_RET="0" 
		and t2.FHO_RET ="0" and t2.PMS_RET="0" and t2.DAT_RET ="0" 
		and t2.NIR_ANO_17 not in ("xxxxxxxxxxxxxxxxx", "XXXXXXXXXXXXXXXXD"))/*On applique le filtre suivant pour liminer des cls de chainage incorrectes*/
ORDER BY t1.eta_num, t1.rsa_num;
quit;

/* on deplace la table dans orauser */
%_eg_conditional_dropds(orauser.mco16c);
data orauser.mco16c;
set mco16c;
run;

/* Fusion avec IR_BEN_R pour avoir les infos sociodmo*/
proc sql;
	create table mco16bcd_ben as
	select t2.ben_idt_ano, t2.ben_sex_cod, t2.ben_nai_ann,2016-input(t2.ben_nai_ann,16.0) as age,
t2.ben_res_dpt, t2.ben_dcd_dte,t1.*
	from orauser.mco16c as t1 inner join oravue.ir_ben_r as t2
	on t1.nir_ano_17 = t2.ben_nir_psa
order by t2.ben_idt_ano;
quit;

/* on deplace la table dans orauser */
%_eg_conditional_dropds(orauser.mco16bcd_ben);
data orauser.mco16bcd_ben;
set mco16bcd_ben;
run;

/*Jointure entre mco16bcd_ben et table des valeurs cim 10 pour le diag principal,diag reli et associ */
proc sql;
	create table mco16bcd_ben as
	select t1.ben_idt_ano, t1.ben_sex_cod, t1.age, t1.ben_res_dpt, t1.ben_dcd_dte, t1.eta_num, 
           t1.rsa_num, t1.exe_soi_dtd, t1.EXE_SOI_DTF,catx("- ", t1.DGN_PAL, t2.cim_lib) as dgn_pal2,
           catx("- ", t1.DGN_REL, t3.cim_lib) as dgn_rel2,
		   catx("- ", t1.ass_dgn, t4.cim_lib) as dgn_ass2
	from orauser.mco16bcd_ben t1 
	LEFT JOIN ORAVAL.IR_CIM_V t2 ON (t1.dgn_pal = t2.cim_cod)
	LEFT JOIN ORAVAL.IR_CIM_V t3 ON (t1.dgn_rel = t3.cim_cod)
	LEFT JOIN ORAVAL.IR_CIM_V t4 ON (t1.ass_dgn = t4.cim_cod)	
	ORDER BY t1.eta_num, t1.rsa_num;
QUIT;
/*Transposition de la table pour obtenir tous les diag associs sur une ligne */
proc transpose data = mco16bcd_ben out = mco16bcd_f (drop=_name_ _label_) prefix = dgn_ass;
   by eta_num rsa_num;
   var dgn_ass2;
run;
/*Table finaleavec une ligne par sjour en hospit*/
data mco16bcd_f1;
merge mco16bcd_ben(keep=ben_idt_ano eta_num rsa_num ben_sex_cod age ben_dcd_dte 
exe_soi_dtd exe_soi_dtf dgn_pal2 dgn_rel2) mco16bcd_f;
by eta_num rsa_num;
year_dc= year(datepart(ben_dcd_dte));
label dgn_pal2="Diagnostic principal" dgn_rel2="Diagnostic reli" year_dc="Anne de dcs";
proc sort nodupkey; by eta_num rsa_num;
run;
/*Nb de rhabdo en 2016*/
proc sql;
select count(ben_idt_ano) as nb_hosp label="Nb de sjours en hospit pour rhabdo, coup de chaleurs ou puisement", 
count(distinct ben_idt_ano) as nb_pers label="Nb de personnes ayant eu au moins un sjour en hospit pour rhabdo, coup de chaleurs ou puisement"
from mco16bcd_f1;
quit;
/*Description socio dmo*/
proc sort data=mco16bcd_f1 out= mco16bcd_f1_unique nodupkey;
by ben_idt_ano;
run;
/*Description socio dmo*/
proc means data=mco16bcd_f1_unique;var age; run;
proc freq data=mco16bcd_f1_unique;table ben_sex_cod year_dc;run;


