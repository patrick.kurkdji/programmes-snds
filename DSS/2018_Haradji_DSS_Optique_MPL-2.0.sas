/* Cr�ation d'une macro-commande permettant de cr�er une table avec :
une ligne = une paire de lunette ou un autre type de prestation en optique m�dicale */
/* Mathis HARADJI - DSS - mathis.haradji@sante.gouv.fr */

%MACRO OPTIQUE (MOISDEB, MOISFIN, ANNEEDEB, ANNEEFIN, N);
 
/* Pour am�liorer la performance de l'ex�cution du programme, nous d�composons la requ�te pour une ann�e de soins donn�e en cr�ant une table par mois d'alimentation (FLX_DIS_DTD).

Nous nommons ces tables avec un suffixe qui indique l'ann�e de soins suivie de l'ordre du mois d'enregistrement (qui va de 1 � 18, 1 pour Janvier de l'ann�e de soins et 18 pour Juin de l'ann�e suivante):
P(k,N) est le suffixe de l'ann�e de soins
 	  P(k,N) = k pour les soins ayant lieu l'ann�e k et enregistr�s l'ann�e k (N=0)
	  P(k+1,N) = k pour les soins ayant lieu l'ann�e k et enregistr�s l'ann�e k+1 (N=1)
Num�ro(l,N) est l'ordre du mois d'enregistrement
	  Num�ro(l,N) = l-1 si le mois d'enregistrement est inclus dans l'ann�e de soins  (N=0)
	  			    l+11 si le mois d'enregistrement est inclus dans l'ann�e suivante (N=1)

Exemple : Pour obtenir les donn�es des soins ex�cut�s en 2017 enregistr�s entre Janvier 2017 et Juin 2018, on lance les deux commandes suivantes :
%OPTIQUE(2,12,2017,2017,0) => Cr�ation de 11 tables pour des soins ex�cut�s en 2017 et enregistr�s entre Janvier 2017 (FLX_DIS_DTD = 01/02/2017) et Novembre 2017 (FLX_DIS_DTD = 01/12/2017).
%OPTIQUE(1,7,2018,2018,1) => Cr�ation de 7 tables  pour des soins ex�cut�s en 2017 et enregistr�s entre D�cembre 2017 (FLX_DIS_DTD = 01/01/2018) et Juin 2018 (FLX_DIS_DTD = 01/07/2018). */

%DO k=&ANNEEDEB %TO &ANNEEFIN;
%LET P&k=%EVAL(&k-&N);
%END;


%DO l=&MOISDEB %TO &MOISFIN;
%LET NUMERO&l=%EVAL((-&l+1)*(&N-1)+(&l+11)*&N);
%END;

/*Extraction des donn�es de remboursements en optique m�dicale � l'aide du code LPP � partir des tables 
ORAVUE.ER_PRS_F t1, ORAVUE.ER_TIP_F t2, ORAREF.NT_LPP t3*/

PROC SQL;
%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
   CREATE TABLE WORK.OPTIQUE_&&P&j&&NUMERO&i AS 
   SELECT 
/*Identification du b�n�ficiaire */
		  t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
/*Variables de jointure entre la table de prestations et la table des codes LPP */
          	t1.FLX_DIS_DTD,
		  	t1.FLX_TRT_DTD,
		  	t1.FLX_EMT_TYP,
		  	t1.FLX_EMT_NUM,
		  	t1.FLX_EMT_ORD,
		  	t1.ORG_CLE_NUM,
		  	t1.DCT_ORD_NUM,
		  	t1.PRS_ORD_NUM,
		  	t1.REM_TYP_AFF,
/*Variables utilis�es pour le regroupement de verres et montures en une paire de lunettes */
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.ORG_CLE_NEW,
/*Nature de prestation de r�f�rence */
		  t1.PRS_NAT_REF,
/*Indicateurs de d�penses issus de la table des prestations */
		    t1.PRS_PAI_MNT, 
		  	t1.BSE_REM_MNT, 
          	t1.BSE_REM_BSE,
          	t1.CPL_REM_MNT, 
          	t1.CPL_REM_BSE, 
/*Variables issues de la table affin�e LPP */
		  t2.TIP_PRS_IDE,
		  t2.TIP_ACT_PRU,
		  t2.TIP_PUB_PRX,
		  t2.TIP_ACT_QSN,
		  t2.TIP_ORD_NUM,
/*Variables situant les codes LPP en optique m�dicale dans l'aborescence de la classification des codes LPP */
		   	 t3.LPP_PRS_IDE,
			 t3.LPP_RED_LIB,
		   	 t3.LPP_ARB_SC2,
			 t3.LPP_ARB_SC3,
			 t3.LPP_ARB_SC4,
 			 t3.LPP_ARB_SC5
      FROM ORAVUE.ER_PRS_F t1, ORAVUE.ER_TIP_F t2, ORAREF.NT_LPP t3 /* ORAREF.NT_LPP est le r�f�rentiel de l'ensemble des codes LPP (�quivalent de IR_PHA_R pour la pharmacie, donc mis � jour r�guli�rement en fonction de l'�volution de la classification LPP),  dans lequel nous allons r�cup�rer les codes LPP sp�cifiques � l'optique m�dicale */
/*Filtre pour chaque mois de remboursement sur la date de soins voulue */
      WHERE t1.FLX_DIS_DTD=DHMS(MDY(&i,1,&j),0,0,0) AND (t1.EXE_SOI_DTD BETWEEN DHMS(MDY(1,1,&&P&j),0,0,0) AND DHMS(MDY(12,31,&&P&j),0,0,0)) 
	  AND t1.DPN_QLF NOT IN (71) AND t1.RGO_ASU_NAT IN (10, 30, 40)  AND (t1.RGM_COD NOT BETWEEN 700 AND 799) 
/* Filtre sur les prestations de r�f�rence en optique m�dicale */
	  AND t1.PRS_NAT_REF IN (3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530, 3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3553, 3554, 3555, 3556, 3557, 3581, 3582, 3583, 5101, 5102, 5103, 5104, 5105, 5106, 5107) 
/* Jointure entre la table de prestations et la tables des codes LPP */
			AND t1.FLX_DIS_DTD=DHMS(MDY(&i,1,&j),0,0,0) AND t1.FLX_DIS_DTD=t2.FLX_DIS_DTD AND t1.FLX_TRT_DTD=t2.FLX_TRT_DTD 
			AND t1.FLX_EMT_TYP=t2.FLX_EMT_TYP AND t1.FLX_EMT_NUM=t2.FLX_EMT_NUM AND t1.FLX_EMT_ORD=t2.FLX_EMT_ORD 
			AND t1.ORG_CLE_NUM=t2.ORG_CLE_NUM AND t1.DCT_ORD_NUM=t2.DCT_ORD_NUM AND t1.PRS_ORD_NUM=t2.PRS_ORD_NUM AND t1.REM_TYP_AFF=t2.REM_TYP_AFF 
/* R�cup�ration des codes LPP en optique m�dicale en r�alisant un filtre sur l'arborescence de la classification LPP. Pour l'optique m�dicale, c'est le chapitre 2 (variable LPP_ARB_SC1) du titre 2 (variable LPP_ARB_CHA) */ 
					AND t2.TIP_PRS_IDE=t3.LPP_PRS_IDE AND t3.LPP_ARB_CHA=2 AND t3.LPP_ARB_SC1=2 ;
%END;%END;
QUIT;


/* Suppression des doublons des lignes de la table de prestation apparus suite � la jointure avec la tables des codes LPP
gr�ce � la cr�ation de variables quantifiant la quantit� de verres et de montures factur�s par ligne de la table des prestations */

PROC SQL ;
%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
CREATE TABLE WORK.OPTIQUE_CLE_&&P&j&&NUMERO&i AS
SELECT    
/*Variables issues de la table de prestation caract�risant une ligne distincte de cette table 
et en fonction desquelles sera quantifi� le nombre de verres ou montures associ�s */
		  t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
          	t1.FLX_DIS_DTD,
		  	t1.FLX_TRT_DTD,
		  	t1.FLX_EMT_TYP,
		  	t1.FLX_EMT_NUM,
		  	t1.FLX_EMT_ORD,
		  	t1.ORG_CLE_NUM,
		  	t1.DCT_ORD_NUM,
		  	t1.PRS_ORD_NUM,
		  	t1.REM_TYP_AFF,
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.ORG_CLE_NEW,
		  t1.PRS_NAT_REF,
		    t1.PRS_PAI_MNT, 
		  	t1.BSE_REM_MNT, 
          	t1.BSE_REM_BSE,
          	t1.CPL_REM_MNT, 
          	t1.CPL_REM_BSE, 
/*Calcul du nombre de verres par ligne de la table de prestation */
		  	(SUM(CASE
		  WHEN (t1.LPP_ARB_SC2=1 AND t1.LPP_ARB_SC4<3 AND t1.LPP_ARB_SC5=1)/* OR (t1.LPP_ARB_SC2=5 AND t1.LPP_ARB_SC4=1)*/ /* Condition � rajouter lorsque la nomenclature aura �volu� suite mise en place de la r�forme 100% sant�*/
		  THEN t1.TIP_ACT_QSN
		  ELSE 0
		  END)) AS VERRES_SIMPLES_QTE, /* Nous classons ici dans les verres simples les verres unifocaux */
		  (SUM(CASE
		  WHEN (t1.LPP_ARB_SC2=1 AND t1.LPP_ARB_SC4<3 AND t1.LPP_ARB_SC5=2) /*OR (t1.LPP_ARB_SC2=5 AND t1.LPP_ARB_SC4>1)*/ /* Condition � rajouter lorsque la nomenclature aura �volu� suite mise en place de la r�forme 100% sant�*/
		  THEN t1.TIP_ACT_QSN
		  ELSE 0
		  END)) AS VERRES_COMPLEXES_QTE, /* Nous classons ici dans les verres complexes les verres multifocaux ou progressifs */
/*Calcul du nombre de montures par ligne de la table de prestation */
		  (SUM(CASE
		  WHEN t1.TIP_PRS_IDE IN (2210546,2223342)
		  THEN t1.TIP_ACT_QSN
		  ELSE 0
		  END)) AS MONTURES_QTE,
/*Caract�risation des lignes de la table de prestation concernant de l'optique m�dicale mais pas de lunettes*/
		  (MAX(CASE
		  WHEN t1.LPP_ARB_SC2 IN (2, 3)
		  THEN 1
		  ELSE 0
		  END)) AS TOP_HORS_LUNETTES
FROM WORK.OPTIQUE_&&P&j&&NUMERO&i t1
GROUP BY 
		  t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
          	t1.FLX_DIS_DTD,
		  	t1.FLX_TRT_DTD,
		  	t1.FLX_EMT_TYP,
		  	t1.FLX_EMT_NUM,
		  	t1.FLX_EMT_ORD,
		  	t1.ORG_CLE_NUM,
		  	t1.DCT_ORD_NUM,
		  	t1.PRS_ORD_NUM,
		  	t1.REM_TYP_AFF,
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.PRS_NAT_REF,
		  t1.ORG_CLE_NEW,
		    t1.PRS_PAI_MNT, 
		  	t1.BSE_REM_MNT, 
          	t1.BSE_REM_BSE,
          	t1.CPL_REM_MNT, 
          	t1.CPL_REM_BSE;
%END;%END;
QUIT;

/*Suppression des tables interm�daires */

%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.OPTIQUE_&&P&j&&NUMERO&i;
RUN;
%END;%END;


/*Transformation des tables WORK.OPTIQUE_CLE_ dans lesquelles chaque ligne correspond � 1 ligne de la table des prestations
en des tables WORK.OPTIQUE_JOUR_ dans lesquelles 1 ligne correspond soit � la d�livrance d'une paire de lunettes 
soit � une prestation diff�rente */

PROC SQL ;
%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
CREATE TABLE WORK.OPTIQUE_JOUR_&&P&j&&NUMERO&i AS
SELECT 	 
/*Variables en fonction desquelles on d�termine si l'ensemble des prestations d�livr�es peuvent correspondre � une paire de lunettes */
		  t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.ORG_CLE_NEW,
		  t1.TOP_HORS_LUNETTES,
/*Agr�gation des indicateurs en fonction des variables ci-dessus*/
	   (SUM(CASE
	 WHEN t1.BSE_REM_BSE<>. AND t1.CPL_REM_BSE=.
	 THEN t1.BSE_REM_BSE
	 WHEN t1.CPL_REM_BSE<>. AND t1.BSE_REM_BSE=. 
	 THEN t1.CPL_REM_BSE
	 WHEN t1.BSE_REM_BSE<>. AND t1.CPL_REM_BSE<>.
	 THEN t1.BSE_REM_BSE+t1.CPL_REM_BSE
	 END)) AS BRSS,
	   (SUM(CASE
	 WHEN t1.BSE_REM_MNT<>. AND t1.CPL_REM_MNT=.
	 THEN t1.BSE_REM_MNT
	 WHEN t1.CPL_REM_MNT<>. AND t1.BSE_REM_MNT=. 
	 THEN t1.CPL_REM_MNT
	 WHEN t1.BSE_REM_MNT<>. AND t1.CPL_REM_MNT<>.
	 THEN t1.BSE_REM_MNT+t1.CPL_REM_MNT
	 END)) AS PART_AMO,
	   (SUM(t1.PRS_PAI_MNT)) AS TOTAL_MNT,
	   (SUM(t1.VERRES_SIMPLES_QTE)) AS VS_QTE,
	   (SUM(t1.VERRES_COMPLEXES_QTE)) AS VC_QTE,
	   (SUM(t1.MONTURES_QTE)) AS MT_QTE,
	   (MAX(CASE
	   WHEN t1.VERRES_SIMPLES_QTE<>0
	   THEN 1
	   ELSE 0
	   END)) AS TOP_OPTIQUE_SIMPLE,
	   (MAX(CASE
	   WHEN t1.VERRES_COMPLEXES_QTE<>0
	   THEN 1
	   ELSE 0
	   END)) AS TOP_OPTIQUE_COMPLEXE
FROM WORK.OPTIQUE_CLE_&&P&j&&NUMERO&i t1
GROUP BY t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.ORG_CLE_NEW,
		  t1.TOP_HORS_LUNETTES;
%END;%END;
QUIT;

/*Suppression des tables interm�daires */

%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.OPTIQUE_CLE_&&P&j&&NUMERO&i;
RUN;
%END;%END;

/*Cr�ation des variables relatives aux paires de lunettes*/

PROC SQL ;
%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
CREATE TABLE WORK.LUNETTES_&&P&j&&NUMERO&i AS
SELECT    t1.BEN_NIR_PSA,
 		  t1.BEN_RNG_GEM,
		  t1.EXE_SOI_DTD,
		  t1.EXE_SOI_DTF,
		  t1.PFS_EXE_NUM,
		  t1.PFS_PRE_NUM,
		  t1.ETB_PRE_FIN,
		  t1.RGO_REM_TAU,
		  t1.ORG_CLE_NEW,
		  t1.TOP_HORS_LUNETTES,
		(CASE 
		WHEN (t1.VS_QTE+t1.VC_QTE)=2*t1.MT_QTE AND t1.TOP_HORS_LUNETTES=0
		THEN 'LUNETTES'
		WHEN (t1.VS_QTE+t1.VC_QTE)<>2*t1.MT_QTE AND t1.TOP_HORS_LUNETTES=0
		THEN 'COMPLEMENT LUNETTES'
		ELSE 'HORS LUNETTES'
		END) AS TOP_LUNETTES,
		(CASE 
		WHEN (t1.VS_QTE+t1.VC_QTE)=2*t1.MT_QTE AND t1.TOP_HORS_LUNETTES=0
		THEN t1.MT_QTE
		ELSE 0
		END) AS LUNETTES_QTE,
		(CASE 
		WHEN (t1.VS_QTE+t1.VC_QTE)=2*t1.MT_QTE AND t1.TOP_HORS_LUNETTES=0
		THEN t1.TOTAL_MNT/t1.MT_QTE
		ELSE 0
		END) AS LUNETTES_PRIX,
	  t1.TOTAL_MNT,
	  t1.BRSS,
	  t1.PART_AMO,
	  t1.VS_QTE,
	  t1.VC_QTE,
	  t1.MT_QTE,
	  t1.TOP_OPTIQUE_SIMPLE,
	  t1.TOP_OPTIQUE_COMPLEXE
FROM WORK.OPTIQUE_JOUR_&&P&j&&NUMERO&i t1;
%END;%END;
QUIT; 

/*Suppression des tables interm�daires */

%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.OPTIQUE_JOUR_&&P&j&&NUMERO&i;
RUN;
%END;%END;

%MEND;

%OPTIQUE (2, 12, 2017, 2017, 0);
%OPTIQUE (1, 7, 2018, 2018, 1);


/* Cr�ation d'une macro-commande permettant de cr�er deux tables annuelles avec des statistiques descriptives sur l'optique m�dicale */

%MACRO EMPILEMENT (MOISDEB, MOISFIN, ANNEEDEB, ANNEEFIN);

/* Empilement par ann�e des tables mensuelles de remboursement dans lesquelles
1 ligne = 1 paire de lunettes ou une prestation diff�rente */ 

%DO j=&ANNEEDEB %TO &ANNEEFIN;
DATA WORK.LUNETTES_&j;
SET %DO i=&MOISDEB %TO &MOISFIN;
WORK.LUNETTES_&j&i 
%END;;
RUN;
%END;

/*Suppression des tables interm�daires */

%DO i=&MOISDEB %TO &MOISFIN;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.LUNETTES_&j&i;
RUN;
%END;%END;
  
/*Cr�ation d'une table de statistiques descriptives avec la d�pense et le volume agr�g�s par type de prestation en optique m�dicale et type de lunettes*/

PROC SQL;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
CREATE TABLE WORK.SYNTHESE_&j AS
SELECT
t1.TOP_LUNETTES,
t1.TOP_OPTIQUE_SIMPLE,
t1.TOP_OPTIQUE_COMPLEXE,
(SUM(t1.TOTAL_MNT)) AS TOTAL_MNT,
(SUM(t1.BRSS)) AS BRSS,
(SUM(t1.PART_AMO)) AS PART_AMO,
(SUM(t1.LUNETTES_QTE)) AS LUNETTES_QTE
FROM WORK.LUNETTES_&j t1
GROUP BY t1.TOP_LUNETTES,
t1.TOP_OPTIQUE_SIMPLE,
t1.TOP_OPTIQUE_COMPLEXE;
%END;
QUIT;

/* Tri de la table annuelle avec 1 ligne = 1 paire de lunettes  selon le type de lunettes 
(en vue de r�aliser une distribution du prix des lunettes selon le type de lunettes) */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC SORT
DATA=WORK.LUNETTES_&j
OUT=WORK.LUNETTES_TRI_&j;
BY TOP_OPTIQUE_SIMPLE;
RUN;
%END;

/*Suppression des tables interm�daires */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.LUNETTES_&j;
RUN;
%END;


/*Distribution d�taill�e du prix des lunettes selon le type de lunettes avec 1 ligne=1 prix */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC FREQ
DATA=WORK.LUNETTES_TRI_&j (WHERE=(TOP_LUNETTES='LUNETTES' AND LUNETTES_QTE>0 ))
NOPRINT;
WEIGHT LUNETTES_QTE;
TABLE LUNETTES_PRIX / OUTCUM OUT=WORK.LUNETTES_PRIX_&j;
BY TOP_OPTIQUE_SIMPLE;
RUN;
%END;

/*Suppression des tables interm�daires */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.LUNETTES_TRI_&j;
RUN;
%END;

/* Cr�ation d'une variable d�cile en vue de r�aliser une distribution du prix des lunettes par d�cile de prix */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
DATA WORK.LUNETTES_DECILE_&j;
SET WORK.LUNETTES_PRIX_&j;
DECILE=MIN(CEIL(CUM_PCT/10), 10);
RUN;
%END;

/*Suppression des tables interm�daires */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.LUNETTES_PRIX_&j;
RUN;
%END;

/* Distribution du prix des lunettes par d�cile de prix */

PROC SQL;
%DO j=&ANNEEDEB %TO &ANNEEFIN;
CREATE TABLE WORK.LUNETTES_DISTRIBUTION_&j AS
SELECT
t1.TOP_OPTIQUE_SIMPLE,
t1.DECILE,
MAX(t1.LUNETTES_PRIX) AS MAX_DECILE,
SUM(t1.LUNETTES_PRIX*t1.COUNT)/SUM(t1.COUNT) AS MOYENNE_DECILE
FROM WORK.LUNETTES_DECILE_&j t1
GROUP BY t1.TOP_OPTIQUE_SIMPLE, t1.DECILE;
%END;

/*Suppression des tables interm�daires */

%DO j=&ANNEEDEB %TO &ANNEEFIN;
PROC DELETE
DATA=WORK.LUNETTES_DECILE_&j;
RUN;
%END;

%MEND;

%EMPILEMENT (1, 18, 2017, 2017);


