
Variable BO avec formule de Calcul :
----------------------------------

Département Bénéficiaire


=Si([département Affiliation bénéf]="001" ;"01 : Ain")+
Si([département Affiliation bénéf]="002";"02 : Aisne")+
Si([département Affiliation bénéf]="003";"03 : Allier")+
Si([département Affiliation bénéf]="004";"04 : Alpes-de-Haute-Pce")+
Si([département Affiliation bénéf]="005";"05 : Hautes-Alpes")+
Si([département Affiliation bénéf]="006";"06 : Alpes-Maritimes")+
Si([département Affiliation bénéf]="007";"07 : Ardèche")+
Si([département Affiliation bénéf]="008";"08 : Ardennes")+
Si([département Affiliation bénéf]="009";"09 : Ariège")+
Si([département Affiliation bénéf]="010";"10 : Aube")+
Si([département Affiliation bénéf]="011";"11 : Aude")+
Si([département Affiliation bénéf]="012";"12 : Aveyron")+
Si([département Affiliation bénéf]="013";"13 : Bouches-du-Rhône")+
Si([département Affiliation bénéf]="014";"14 : Calvados")+
Si([département Affiliation bénéf]="015";"15 : Cantal")+
Si([département Affiliation bénéf]="016";"16 : Charente")+
Si([département Affiliation bénéf]="017";"17 : Charente-Maritime")+
Si([département Affiliation bénéf]="018";"18 : Cher")+
Si([département Affiliation bénéf]="019";"19 : Corrèze")+
Si([département Affiliation bénéf]="02A";"2A : Corse-du-Sud")+
Si([département Affiliation bénéf]="02B";"2B : Haute-Corse")+
Si([département Affiliation bénéf]="021";"21 : Côte-d'Or")+
Si([département Affiliation bénéf]="022";"22 : Côtes-d'Armor")+
Si([département Affiliation bénéf]="023";"23 : Creuse")+
Si([département Affiliation bénéf]="024";"24 : Dordogne")+
Si([département Affiliation bénéf]="025";"25 : Doubs")+
Si([département Affiliation bénéf]="026";"26 : Drôme")+
Si([département Affiliation bénéf]="027";"27 : Eure")+
Si([département Affiliation bénéf]="028";"28 : Eure-et-Loir")+
Si([département Affiliation bénéf]="029";"29 : Finistère")+
Si([département Affiliation bénéf]="030";"30 : Gard")+
Si([département Affiliation bénéf]="031";"31 : Haute-Garonne")+
Si([département Affiliation bénéf]="032";"32 : Gers")+
Si([département Affiliation bénéf]="033";"33 : Gironde")+
Si([département Affiliation bénéf]="034";"34 : Hérault")+
Si([département Affiliation bénéf]="035";"35 : Ille-et-Vilaine")+
Si([département Affiliation bénéf]="036";"36 : Indre")+
Si([département Affiliation bénéf]="037";"37 : Indre-et-Loire")+
Si([département Affiliation bénéf]="038";"38 : Isère")+
Si([département Affiliation bénéf]="039";"39 : Jura")+
Si([département Affiliation bénéf]="040";"40 : Landes")+
Si([département Affiliation bénéf]="041";"41 : Loir-et-Cher")+
Si([département Affiliation bénéf]="042";"42 : Loire")+
Si([département Affiliation bénéf]="043";"43 : Haute-Loire")+
Si([département Affiliation bénéf]="044";"44 : Loire-Atlantique")+
Si([département Affiliation bénéf]="045";"45 : Loiret")+
Si([département Affiliation bénéf]="046";"46 : Lot")+
Si([département Affiliation bénéf]="047";"47 : Lot-et-Garonne")+
Si([département Affiliation bénéf]="048";"48 : Lozère")+
Si([département Affiliation bénéf]="049";"49 : Maine-et-Loire")+
Si([département Affiliation bénéf]="050";"50 : Manche")+
Si([département Affiliation bénéf]="051";"51 : Marne")+
Si([département Affiliation bénéf]="052";"52 : Haute-Marne")+
Si([département Affiliation bénéf]="053";"53 : Mayenne")+
Si([département Affiliation bénéf]="054";"54 : Meurthe-et-Moselle")+
Si([département Affiliation bénéf]="055";"55 : Meuse")+
Si([département Affiliation bénéf]="056";"56 : Morbihan")+
Si([département Affiliation bénéf]="057";"57 : Moselle")+
Si([département Affiliation bénéf]="058";"58 : Nièvre")+
Si([département Affiliation bénéf]="059";"59 : Nord")+
Si([département Affiliation bénéf]="060";"60 : Oise")+
Si([département Affiliation bénéf]="061";"61 : Orne")+
Si([département Affiliation bénéf]="062";"62 : Pas-de-Calais")+
Si([département Affiliation bénéf]="063";"63 : Puy-de-Dôme")+
Si([département Affiliation bénéf]="064";"64 : Pyrénées-Atlantiques")+
Si([département Affiliation bénéf]="065";"65 : Hautes-Pyrénées")+
Si([département Affiliation bénéf]="066";"66 : Pyrénées-Orientales")+
Si([département Affiliation bénéf]="067";"67 : Bas-Rhin")+
Si([département Affiliation bénéf]="068";"68 : Haut-Rhin")+
Si([département Affiliation bénéf]="069";"69 : Rhône")+
Si([département Affiliation bénéf]="070";"70 : Haute-Saône")+
Si([département Affiliation bénéf]="071";"71 : Saône-et-Loire")+
Si([département Affiliation bénéf]="072";"72 : Sarthe")+
Si([département Affiliation bénéf]="073";"73 : Savoie")+
Si([département Affiliation bénéf]="074";"74 : Haute-Savoie")+
Si([département Affiliation bénéf]="075";"75 : Paris")+
Si([département Affiliation bénéf]="076";"76 : Seine-Maritime")+
Si([département Affiliation bénéf]="077";"77 : Seine-et-Marne")+
Si([département Affiliation bénéf]="078";"78 : Yvelines")+
Si([département Affiliation bénéf]="079";"79 : Deux-Sèvres")+
Si([département Affiliation bénéf]="080";"80 : Somme")+
Si([département Affiliation bénéf]="081";"81 : Tarn")+
Si([département Affiliation bénéf]="082";"82 : Tarn-et-Garonne")+
Si([département Affiliation bénéf]="083";"83 : Var")+
Si([département Affiliation bénéf]="084";"84 : Vaucluse")+
Si([département Affiliation bénéf]="085";"85 : Vendée")+
Si([département Affiliation bénéf]="086";"86 : Vienne")+
Si([département Affiliation bénéf]="087";"87 : Haute-Vienne")+
Si([département Affiliation bénéf]="088";"88 : Vosges")+
Si([département Affiliation bénéf]="089";"89 : Yonne")+
Si([département Affiliation bénéf]="090";"90 : Territoire de Belfort")+
Si([département Affiliation bénéf]="091";"91 : Essonne")+
Si([département Affiliation bénéf]="092";"92 : Hauts-de-Seine")+
Si([département Affiliation bénéf]="093";"93 : Seine-Saint-Denis")+
Si([département Affiliation bénéf]="094";"94 : Val-de-Marne")+
Si([département Affiliation bénéf]="095";"95 : Val-d'Oise")+
Si([département Affiliation bénéf]="971";"971 : Guadeloupe")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="971";"971 : Guadeloupe")+
Si([département Affiliation bénéf]="972";"972 : Martinique")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="972";"972 : Martinique")+
Si([département Affiliation bénéf]="973";"973 : Guyane")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="973";"973 : Guyane")+
Si([département Affiliation bénéf]="974";"974 : La Réunion")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="974";"974 : La Réunion")+Si([département Affiliation bénéf]="976";"976 : Mayotte")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="976";"976 : Mayotte")+Si([département Affiliation bénéf]="977";"977 : Saint Barthélémy")+Si( [département Affiliation bénéf]="097" Et Droite([Organisme d'Affiliation];3)="977";"977 : Saint Barthélémy")+Si( [département Affiliation bénéf] DansListe("209";"020") Et Droite([Organisme d'Affiliation];3)="201";"2A : Corse du Sud")+Si( [département Affiliation bénéf] DansListe("209";"020") Et Droite([Organisme d'Affiliation];3)="202";"2B : Haute Corse")