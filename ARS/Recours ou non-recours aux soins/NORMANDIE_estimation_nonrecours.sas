option mprint symbolgen;
/********************************************************************/
/****Estimation  du non recours � partir de la m�thodo de l'observatoire des fragilites 
-pas de soin sur 24 mois 
-on ne consid�re pas les individus arriv�s au cours de ces 24 mois
-on enleve les moins de 16 ans

Attention IR_BEN_R n'est pas un r�f�rentiel des b�n�ficiaires, les non-consommants de la MSA et RSI n'y sont pas ! on extrapole donc le comportement des b�n�ficiaires du r�gime g�n�ral. 

--C�line LEROY  ARS NORMANDIE profil 84
  */
/*****************************************************************/

/***Entr�e des param�tres ***/
* Liste du ou des d�partement entre cote avec virgule si plusieurs;
%LET DEPT ='014','027','050','061','076';
*%LET DEPT ='018','028','036','037','041','045';


* FIN des param�tres en entr�e;


/********************************************************************************/
/**Partie 1 : non recours aux soins 						 */
/*********************************************************************************/

PROC SQL;
   CREATE TABLE WORK.POP_REF AS 
   SELECT t1.BEN_RES_DPT, 
          t1.BEN_RES_COM, 
           /* (COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then t1.BEN_IDT_ANO end))) AS NB_CONSO_REF,*/ /* On se sert du  TOP Conso */
			  /*(COUNT(DISTINCT(t1.BEN_NIR_PSA ||put(t1.BEN_RNG_GEM,2.))))as NB_TOT_REF,*/
				(count(distinct t1.BEN_IDT_ANO)) as NB_TOT_REF2
      FROM ORAVUE.IR_BEN_R t1
      WHERE t1.BEN_RES_DPT in(&DEPT)
	  AND (SUBSTR(t1.ORG_AFF_BEN,1,3) in('01C','06A','07A','10A','90A') 
	  				OR (SUBSTR(t1.ORG_AFF_BEN,1,3)='01M' AND SUBSTR(t1.ORG_AFF_BEN,7,3)in('619','537','599','619','516','601','603','604'))
	/* attention pas MSA/RSI */ 				) 
	 AND (t1.BEN_NAI_ANN < '2002' AND t1.ben_nai_ann>'1600' /* Elimination des naissances apr�s 2002*/ /*or t1.ben_NAI_ann>'2012'*/)
	 AND (t1.BEN_DCD_AME > '201612' OR t1.BEN_DCD_AME = '160001') /* Elimination  d�c�des ant�rieurs � 2017 en gardant les vivants!!*/
/*AND T1.max_trt_dtd> to_date('20161231','YYYYMMDD') */
	  AND BEN_DTE_INS <'31DEC2015:0:0:0'dt
GROUP BY t1.BEN_RES_DPT,
               t1.BEN_RES_COM;
QUIT;


PROC SQL;
   CREATE TABLE WORK.POP_CONSO AS 
   SELECT t1.BEN_RES_DPT, 
          t1.BEN_RES_COM, 
            /*(COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then (t1.BEN_NIR_PSA ||put(t1.BEN_RNG_GEM,2.)) end))) AS NB_CONSO_REF_2016,/* On se sert du  TOP Conso */
			(COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then ben_idt_ano end))) AS NB_CONSO_REF_2016bis
			/*(COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_TOT_REF*/
      FROM ORAVUE.IR_BEN_R t1
      WHERE t1.BEN_RES_DPT in(&DEPT)
	  AND (SUBSTR(t1.ORG_AFF_BEN,1,3) in('01C','06A','07A','10A','90A') 
	  				OR (SUBSTR(t1.ORG_AFF_BEN,1,3)='01M' AND SUBSTR(t1.ORG_AFF_BEN,7,3)in('619','537','599','619','516','601','603','604'))
	/* attention pas MSA/RSI */ 				) 
	  AND (t1.BEN_NAI_ANN < '2002' AND t1.ben_nai_ann>'1600'/* Elimination des naissances apr�s 2002*/ /*or t1.ben_NAI_ann>'2012'*/)
	 AND (t1.BEN_DCD_AME > '201612' OR t1.BEN_DCD_AME = '160001') /* Elimination  d�c�des ant�rieurs � 2017 en gardant les vivants!!*/
   AND t1.max_trt_dtd > '31DEC2015:0:0:0'dt  
   AND BEN_DTE_INS <'31DEC2015:0:0:0'dt
GROUP BY t1.BEN_RES_DPT,
               t1.BEN_RES_COM;
QUIT;

 proc sql;
create table pop_conso_tot as select * 
from  pop_ref a,pop_conso b
where a.ben_res_dpt=b.ben_res_dpt
and a.ben_res_com=b.ben_res_com;
run;




* Note En infra d�partementales on �limine les militaires '08A';

/*** SLM 
la mutuelle g�n�rale LMG 01MXXX619,
 la mutuelle g�n�rale de la police MGP 01MXXX537,
 MFP service MFPS 01MXXX599,
 la mutuelle nationale des hospitaliers MNH 01MXXX619,
 Harmonie fonction publique HFP 01MXXX516,
 la mutuelle des �tudiants LMDE (depuis janvier 2017) 01MXXX601,
 la caisse d�assurance maladie des industries �lectriques et gazi�res CAMIEG 01MXXX603,
 la mutuelle du minist�re de l�int�rieur Int�riale 01MXXX604,

***/




* Correction des code communes erron�s;
* m�me manip que pr�c�demment;
PROC SQL;
   CREATE TABLE sasdata1.POP_REF_CORR AS 
   SELECT  distinct
                 case when t2.CODE_INSEE is not null then t2.CODE_INSEE else substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM end as cod_comm , 
				  t2.LIBELLE_DE_LA_COMMUNE, 
                  /*sum(t1.NB_CONSO_REF_2016) AS NB_CONSO_REF_2016, */
                 /* sum(t1.NB_TOT_REF) AS NB_TOT_REF,*/
				sum(t1.NB_CONSO_REF_2016bis) AS NB_CONSO_REF_2016bis, 
				  sum(t1.NB_TOT_REF2) AS NB_TOT_REF2
      FROM WORK.POP_CONSO_tot t1
           LEFT JOIN RFCOMMUN.T_FIN_GEO_LOC_FRANCE t2 ON (substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM = t2.CODE_JOINTURE)
group by case when t2.CODE_INSEE is not null then t2.CODE_INSEE else t1.BEN_RES_DPT||t1.BEN_RES_COM end ,
    t2.LIBELLE_DE_LA_COMMUNE;
QUIT;




