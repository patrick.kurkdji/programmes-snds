option mprint symbolgen;
/********************************************************************/
/****Estimation  du taux de recours par commune au MG sur 2017    */
/*****************************************************************/

/***Entr�e des param�tres ***/
* Liste du ou des d�partement entre cote avec virgule si plusieurs;
%LET DEPT ='014','027','050','061','076';
*%LET DEPT ='018','028','036','037','041','045';


* FIN des param�tres en entr�e;

/**************************************************************************/
/**Partie 1 : calcul  des benef MG et des consommants de la Table PRS */
/**************************************************************************/

/* Num�rateur : Nb de benef ayant eu recours au MG (Lib�raux et Centre de Sant�)*/ 

PROC SQL;
%connectora;
create table BEN_CONS_MG as
select * from connection to oracle      (
SELECT distinct BEN_RES_DPT,
BEN_RES_COM ,
  count(distinct ben_idt_ano) as nb_patient_mg
FROM  ns_prs_f
WHERE  EXE_SOI_DTD BETWEEN to_date('20170101','YYYYMMDD') AND to_date('20171231','YYYYMMDD')
and (ETE_IND_TAA not in (1)  OR ETE_IND_TAA is null )/* on enleve Fides*/
  AND prs_nat_ref not in (1124, 1130, 1147, 1128, 1144,1915, 1916, 1917,
                        1990, 1991,1992,1993,1994,1995,1996,1997,1998,1999,1718,1145, 1146,1142,1143,
                        1628,1629,1630,1631,1632,1633,1634,1635,1636,1637,1638,1639,1150, 1160, 1161, 1162,
                         1640, 1641, 1642,1956,1951,1952) /* Elimination des forfaits  autres prestations n'�tant pas du soins (cf maquette Stat mens) */
AND exe_soi_amd BETWEEN  '201701' AND '201712'  
AND pse_spe_cod in (1,22,23)
AND BEN_RES_DPT in (&DEPT)
group by BEN_RES_DPT,BEN_RES_COM);
disconnect from oracle;
quit ;

/* D�nominateur :  Nb de benef ayant eu une prestation de rembours� y compris FIDES  */ 
PROC SQL;
%connectora;
create table BEN_CONS_PRS as
select * from connection to oracle      (
SELECT distinct BEN_RES_DPT,
BEN_RES_COM ,
  count(distinct ben_idt_ano) as nb_patient_conso
FROM  ns_prs_f
WHERE  EXE_SOI_DTD BETWEEN to_date('20170101','YYYYMMDD') AND to_date('20171231','YYYYMMDD')
AND exe_soi_amd BETWEEN  '201701' AND '201712'  
AND BEN_RES_DPT in (&DEPT)
group by BEN_RES_DPT,BEN_RES_COM);
disconnect from oracle;
quit ;

* Correction des code communes erron�s sur les 2 tables ;
* Utilisation de la table T_FIN_GEO_LOC (ARSIF) pour r�afffecter les codes INSEE;
* On utilise la LEFT JOIN pour ne pas �liminer �ventuellement des  codes non connus;
* On r�cup�re aussi le libell� de la commune; 

PROC SQL;
   CREATE TABLE WORK.BEN_CONS_MG_CORR AS 
   SELECT  distinct
                 case when t2.CODE_INSEE is not null then t2.CODE_INSEE else substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM end as cod_comm , 
				  t2.LIBELLE_DE_LA_COMMUNE, 
                  sum(t1.nb_patient_mg) AS nb_patient_mg 
      FROM WORK.BEN_CONS_MG t1
           LEFT JOIN PRFEXT.T_FIN_GEO_LOC_FRANCE t2 ON (substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM = t2.CODE_JOINTURE)
	group by case when t2.CODE_INSEE is not null then t2.CODE_INSEE else t1.BEN_RES_DPT||t1.BEN_RES_COM end ,
    t2.LIBELLE_DE_LA_COMMUNE;
QUIT;

PROC SQL;
   CREATE TABLE WORK.BEN_CONS_PRS_CORR AS 
   SELECT  distinct
                 case when t2.CODE_INSEE is not null then t2.CODE_INSEE else substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM end as cod_comm , 
				  t2.LIBELLE_DE_LA_COMMUNE, 
                  sum(t1.nb_patient_conso) AS nb_patient_conso 
      FROM WORK.BEN_CONS_PRS t1
           LEFT JOIN PRFEXT.T_FIN_GEO_LOC_FRANCE t2 ON (substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM = t2.CODE_JOINTURE)
	group by case when t2.CODE_INSEE is not null then t2.CODE_INSEE else t1.BEN_RES_DPT||t1.BEN_RES_COM end ,
    t2.LIBELLE_DE_LA_COMMUNE;
QUIT;



/********************************************************************************/
/**Partie 2 : calcul  des benef consommants  et non consommant sur IR_IBA_R      */
/* on ne retient que les R�gimes et SLM qui reemontent l'info de Non consommants */
/*********************************************************************************/

* Note En infra d�partementales on �limine les militaires '08A';

/*** SLM 
la mutuelle g�n�rale LMG 01MXXX619,
 la mutuelle g�n�rale de la police MGP 01MXXX537,
 MFP service MFPS 01MXXX599,
 la mutuelle nationale des hospitaliers MNH 01MXXX619,
 Harmonie fonction publique HFP 01MXXX516,
 la mutuelle des �tudiants LMDE (depuis janvier 2017) 01MXXX601,
 la caisse d�assurance maladie des industries �lectriques et gazi�res CAMIEG 01MXXX603,
 la mutuelle du minist�re de l�int�rieur Int�riale 01MXXX604,

***/
PROC SQL;
   CREATE TABLE WORK.POP_CONSO_NONCONSO AS 
   SELECT t1.BEN_RES_DPT, 
          t1.BEN_RES_COM, 
            (COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then t1.BEN_IDT_ANO end))) AS NB_CONSO_REF, /* On se sert du  TOP Conso */
			(COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_TOT_REF
      FROM ORAVUE.IR_IBA_R t1
      WHERE t1.BEN_RES_DPT in(&DEPT)
	  AND (SUBSTR(t1.ORG_AFF_BEN,1,3) in('01C','06A','07A','10A','90A') 
	  				OR (SUBSTR(t1.ORG_AFF_BEN,1,3)='01M' AND SUBSTR(t1.ORG_AFF_BEN,7,3)in('619','537','599','619','516','601','603','604'))
					) 
	 AND t1.BEN_NAI_ANN < '2018' /* Elimination des naissances 2018*/
	 AND (t1.BEN_DCD_AME > '201612' OR t1.BEN_DCD_AME = '160001') /* Elimination  d�c�des ant�rieurs � 2017 en gardant les vivants!!*/
     GROUP BY t1.BEN_RES_DPT,
               t1.BEN_RES_COM;
QUIT;

* Correction des code communes erron�s;
* m�me manip que pr�c�demment;
PROC SQL;
   CREATE TABLE WORK.POP_REF_CORR AS 
   SELECT  distinct
                 case when t2.CODE_INSEE is not null then t2.CODE_INSEE else substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM end as cod_comm , 
				  t2.LIBELLE_DE_LA_COMMUNE, 
                  sum(t1.NB_CONSO_REF) AS NB_CONSO_REF , 
                  sum(t1.NB_TOT_REF) AS NB_TOT_REF   
      FROM WORK.POP_CONSO_NONCONSO t1
           LEFT JOIN PRFEXT.T_FIN_GEO_LOC_FRANCE t2 ON (substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM = t2.CODE_JOINTURE)
	group by case when t2.CODE_INSEE is not null then t2.CODE_INSEE else t1.BEN_RES_DPT||t1.BEN_RES_COM end ,
    t2.LIBELLE_DE_LA_COMMUNE;
QUIT;

* On rassemble tous les r�sultats en 1 table par un MERGE;
proc sort data=BEN_CONS_MG_CORR;
	by cod_comm libelle_de_la_commune;
run;
proc sort data=BEN_CONS_PRS_CORR;
	by cod_comm libelle_de_la_commune;
run;
proc sort data=POP_REF_CORR;
	by cod_comm libelle_de_la_commune;
run;


data result;
merge POP_REF_CORR BEN_CONS_PRS_CORR BEN_CONS_MG_CORR;
	by cod_comm libelle_de_la_commune;
	est_txrecours=(nb_patient_mg/nb_patient_conso)*( NB_CONSO_REF/NB_TOT_REF) ; * Le taux de recours est le produit des 2 taux;
    format est_txrecours nlpct20.1;
run;
