/* ---------------------------------------------------------------------------------------------------------------------------
------------------------------- Maternit� Nord Mayenne - Partie PMSI - 1er semestre 2017 -----------------------------------
--------------------------------------------------------------------------------------------------------------------------- */


/* ------------------------------------- PARTIE 1 : ACTES EN CMD 14 (VERSANT FEMMES) ------------------------------------- */

/* Cr�ation de la table des s�jours en CMD 14 pour les patientes de la zone de recrutement */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM AS 
   SELECT t1.ETA_NUM, t1.POI_NAI, t1.NBR_SUP_NN1, t1.NBR_SUP_NN2, t1.NBR_SUP_NN3, 
          t1.AGE_ANN, t1.AGE_GES, t1.GRG_GHM, t1.RSA_NUM, t1.BDI_DEP, t1.BDI_COD, 
          (SUBSTR(t1.GRG_GHM, 1, 2)) AS cmd, (SUBSTR(t1.GRG_GHM, 1, 5)) AS GHM_court, 
          t1.RSS_NUM, t1.SOR_ANN, t1.SOR_MOI, t1.DGN_PAL
      FROM ORAVUE.T_MCO17B t1
      WHERE (CALCULATED cmd) = '14'
          AND t1.BDI_COD IN ('53100','53300','53700','53440','53120','53160','53500','53600','53250','53110','53470','53640');
QUIT;

/* On rapproche de la table du chainage des patients */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2 AS 
   SELECT t1.NIR_ANO_17,
          t2.AGE_ANN, t2.AGE_GES, t2.CMD, t2.DGN_PAL, t2.ETA_NUM, t2.GHM_COURT, t2.GRG_GHM, 
          t2.NBR_SUP_NN1, t2.NBR_SUP_NN2, t2.NBR_SUP_NN3, t2.POI_NAI, t2.RSA_NUM, t2.RSS_NUM, t2.SOR_ANN, t2.SOR_MOI, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'CH Nord Mayenne (1)'
			   WHEN t2.ETA_NUM='530000371' THEN 'CH Laval (2A)'
			   WHEN t2.ETA_NUM='350000030' THEN 'CH Foug�res (2A)'
			   WHEN t2.ETA_NUM='610780082' THEN 'CHIC Alen�on-Mamers (2A)'
			   WHEN t2.ETA_NUM='720000025' THEN 'CH Le Mans (3)'
               ELSE 'Autre maternit�'
            END) AS Mater, 
          (CASE  
               WHEN t2.AGE_ANN<20 THEN 'Moins de 20 ans'
               WHEN t2.AGE_ANN<25 THEN '20-24 ans'
               WHEN t2.AGE_ANN<30 THEN '25-29 ans'
               WHEN t2.AGE_ANN<35 THEN '30-34 ans'
               WHEN t2.AGE_ANN<40 THEN '35-39 ans'
               ELSE '40 ans et plus'
            END) AS Classe_age_mere, 
          (CASE  
               WHEN t2.AGE_GES<28 THEN 'Moins de 28 semaines'
               WHEN t2.AGE_GES<37 THEN '28-37 semaines'
               WHEN t2.AGE_GES<41 THEN '37-41 semaines'
               ELSE '41 semaines et plus'
            END) AS Classe_age_ges, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'NM'
               ELSE 'Pas_NM'
            END) AS NM, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'CH Nord Mayenne (1)'
               WHEN t2.ETA_NUM='530000371' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='350000030' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='610780082' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='720000025' THEN 'Niveau 3'
			   ELSE 'Autre maternit�'
            END
            ) AS Mater_niveau
      FROM ORAVUE.T_MCO17C t1, ORAUSER.CMD14_ZONE_RECRU_NM t2
      WHERE (t1.ETA_NUM = t2.ETA_NUM AND t1.RSA_NUM = t2.RSA_NUM AND t1.SOR_ANN = t2.SOR_ANN AND t1.SOR_MOI = t2.SOR_MOI);
QUIT;

/* Calcul des indicateurs */
/* 1. Nombre total de femmes */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND1;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND1 AS 
   SELECT (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD14_ZONE_RECRU_NM2 t1;
QUIT;
/* 2. R�partition des actes en CMD14 par maternit� */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND2;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND2 AS 
   SELECT t1.Mater, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17, 
          (MEAN(t1.AGE_ANN)) AS MEAN_of_AGE_ANN, 
          (MEAN(t1.AGE_GES)) AS MEAN_of_AGE_GES
      FROM ORAUSER.CMD14_ZONE_RECRU_NM2 t1
      GROUP BY t1.Mater
      ORDER BY COUNT_DISTINCT_of_NIR_ANO_17 DESC;
QUIT;
/* 3. D�tail par �ge des femmes */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND3;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND3 AS 
   SELECT t1.NM, t1.Classe_age_mere, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD14_ZONE_RECRU_NM2 t1
      GROUP BY t1.NM, t1.Classe_age_mere
      ORDER BY t1.NM;
QUIT;
/* 4. D�tail par dur�e moyenne de gestation des femmes */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND4;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND4 AS 
   SELECT t1.NM, t1.Classe_age_ges,
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD14_ZONE_RECRU_NM2 t1
      GROUP BY t1.NM, t1.Classe_age_ges
      ORDER BY t1.NM;
QUIT;
/* 5. Bonus : R�partition des actes en CMD14 effectu�s � la PSSL avec un �ge gestationnel de moins de 37 semaines par GHM */
PROC SQL;
   DROP TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND5;
   CREATE TABLE ORAUSER.CMD14_ZONE_RECRU_NM2_IND5 AS 
   SELECT t1.GRG_GHM, t1.Classe_age_ges,
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD14_ZONE_RECRU_NM2 t1
	  WHERE NM='NM'
	     AND t1.Classe_age_ges IN ('Moins de 28 semaines','28-37 semaines')
      GROUP BY t1.GRG_GHM, t1.Classe_age_ges
      ORDER BY t1.Classe_age_ges;
QUIT;

/* ------------------------------------ PARTIE 2 : ACTES EN CMD 15 (VERSANT ENFANTS) ------------------------------------- */

/* Cr�ation de la table des s�jours en CMD 15 pour les patientes de la zone de recrutement */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM AS 
   SELECT t1.ETA_NUM, t1.POI_NAI, t1.NBR_SUP_NN1, t1.NBR_SUP_NN2, t1.NBR_SUP_NN3, t1.AGE_ANN, t1.AGE_GES, t1.GRG_GHM, 
          t1.RSA_NUM, t1.BDI_DEP, t1.BDI_COD, 
          (SUBSTR(t1.GRG_GHM, 1, 2)) AS cmd, (SUBSTR(t1.GRG_GHM, 1, 5)) AS GHM_court, 
          t1.RSS_NUM, t1.SOR_ANN, t1.SOR_MOI, t1.DGN_PAL
      FROM ORAVUE.T_MCO17B t1
      WHERE (CALCULATED cmd) = '15'
          AND t1.BDI_COD IN ('53100','53300','53700','53440','53120','53160','53500','53600','53250','53110','53470','53640');
QUIT;

/* On rapproche de la table du chainage des patients */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2 AS 
   SELECT t1.NIR_ANO_17,
          t2.AGE_ANN, t2.AGE_GES, t2.CMD, t2.DGN_PAL, t2.ETA_NUM, t2.GHM_COURT, t2.GRG_GHM, t2.BDI_DEP, t2.BDI_COD, 
          t2.NBR_SUP_NN1, t2.NBR_SUP_NN2, t2.NBR_SUP_NN3, t2.POI_NAI, t2.RSA_NUM, t2.RSS_NUM, t2.SOR_ANN, t2.SOR_MOI, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'CH Nord Mayenne (1)'
			   WHEN t2.ETA_NUM='530000371' THEN 'CH Laval (2A)'
			   WHEN t2.ETA_NUM='350000030' THEN 'CH Foug�res (2A)'
			   WHEN t2.ETA_NUM='610780082' THEN 'CHIC Alen�on-Mamers (2A)'
			   WHEN t2.ETA_NUM='720000025' THEN 'CH Le Mans (3)'
               ELSE 'Autre maternit�'
            END) AS Mater, 
          (CASE  
               WHEN t2.POI_NAI<2500 THEN 'Moins de 2,5 kg'
               WHEN t2.POI_NAI<3000 THEN '2,5-3 kg'
               WHEN t2.POI_NAI<3500 THEN '3-3,5 kg'
               WHEN t2.POI_NAI<4000 THEN '3,5-4 kg'
               ELSE '4 kg et plus'
            END) AS Classe_poids_naiss, 
          (CASE  
               WHEN t2.POI_NAI<2000 THEN 'Moins de 2 kg'
               WHEN t2.POI_NAI<3000 THEN '2-3 kg'
               WHEN t2.POI_NAI<3500 THEN '3-3,5 kg'
               WHEN t2.POI_NAI<4000 THEN '3,5-4 kg'
               ELSE '4 kg et plus'
            END) AS Classe_poids_naiss_bis, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'NM'
               ELSE 'Pas_NM'
            END) AS NM, 
          (CASE  
               WHEN t2.ETA_NUM='530000074' THEN 'CH Nord Mayenne (1)'
               WHEN t2.ETA_NUM='530000371' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='350000030' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='610780082' THEN 'Niveau 2'
			   WHEN t2.ETA_NUM='720000025' THEN 'Niveau 3'
			   ELSE 'Autre maternit�'
            END
            ) AS Mater_niveau, 
          (CASE  
               WHEN t2.NBR_SUP_NN1>0 THEN 'NN1'
               ELSE ''
            END) AS NN1, 
          (CASE  
               WHEN t2.NBR_SUP_NN2>0 THEN 'NN2'
               ELSE ''
            END) AS NN2, 
          (CASE  
               WHEN t2.NBR_SUP_NN3>0 THEN 'NN3'
               ELSE ''
            END) AS NN3, 
          (CASE  
               WHEN (CASE  
               WHEN t2.NBR_SUP_NN3>0 THEN 'NN3'
               ELSE ''
            END)='NN3' THEN 'NN3'
               WHEN (CASE  
               WHEN t2.NBR_SUP_NN2>0 THEN 'NN2'
               ELSE ''
            END)='NN2' THEN 'NN2'
               WHEN (CASE  
               WHEN t2.NBR_SUP_NN1>0 THEN 'NN1'
               ELSE ''
            END)='NN1' THEN 'NN1'
            ELSE ''
            END) AS NN
      FROM ORAVUE.T_MCO17C t1, ORAUSER.CMD15_ZONE_RECRU_NM t2
      WHERE (t1.ETA_NUM = t2.ETA_NUM AND t1.RSA_NUM = t2.RSA_NUM AND t1.SOR_ANN = t2.SOR_ANN AND t1.SOR_MOI = t2.SOR_MOI);
QUIT;

/* Calcul des indicateurs */
/* 0. D�tails par code g�ographique de r�sidence */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND0;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND0 AS 
   SELECT t1.BDI_COD, t1.NM, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
      GROUP BY t1.BDI_COD, t1.NM;
QUIT;
/* 1. Nombre total de naissances */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND1;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND1 AS 
   SELECT (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1;
QUIT;
/* 2. R�partition des actes en CMD15 par maternit� */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND2;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND2 AS 
   SELECT t1.Mater, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17, 
          (MEAN(t1.POI_NAI)) AS MEAN_of_POI_NAI
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
      GROUP BY t1.Mater
      ORDER BY COUNT_DISTINCT_of_NIR_ANO_17 DESC;
QUIT;
/* 3. D�tail par poids de naissance */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND3;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND3 AS 
   SELECT t1.NM, t1.Classe_poids_naiss,
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
      GROUP BY t1.NM, t1.Classe_poids_naiss
      ORDER BY t1.NM;
QUIT;
/* 4. D�tail par suppl�ments n�onatologie ou non */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND4;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND4 AS 
   SELECT t1.NM, t1.NN, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
      GROUP BY t1.NM, t1.NN
      ORDER BY t1.NM;
QUIT;
/* 5. D�tail par �tablissement (et type de maternit�) pour les actes effectu�s hors NM */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND5_;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND5_ AS 
   SELECT t1.AGE_ANN, t1.AGE_GES, t1.Classe_poids_naiss, t1.CMD, t1.DGN_PAL, t1.ETA_NUM, t1.GHM_COURT, t1.GRG_GHM, 
          t1.Mater, t1.Mater_niveau, t1.BDI_DEP, t1.BDI_COD, t1.NBR_SUP_NN1, t1.NBR_SUP_NN2, t1.NBR_SUP_NN3, 
          t1.NIR_ANO_17, t1.NN, t1.NN1, t1.NN2, t1.NN3, t1.POI_NAI, t1.NM, 
          t1.RSA_NUM, t1.RSS_NUM, t1.SOR_ANN, t1.SOR_MOI
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
      WHERE t1.NM = 'Pas_NM' AND t1.NN IS NULL;
QUIT;
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND5;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND5 AS 
   SELECT t1.Mater, 
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2_IND5_ t1
      GROUP BY t1.Mater
      ORDER BY COUNT_DISTINCT_of_NIR_ANO_17 DESC;
QUIT;
/* 6. R�partition des actes en CMD15 effectu�s � la PSSL avec un poids de naissance de moins de 2000g par GHM */
PROC SQL;
   DROP TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND6;
   CREATE TABLE ORAUSER.CMD15_ZONE_RECRU_NM2_IND6 AS 
   SELECT t1.GRG_GHM, t1.Classe_poids_naiss_bis,
          (COUNT(DISTINCT(t1.NIR_ANO_17))) AS COUNT_DISTINCT_of_NIR_ANO_17
      FROM ORAUSER.CMD15_ZONE_RECRU_NM2 t1
	  WHERE NM='NM'
	     AND t1.Classe_poids_naiss_bis='Moins de 2 kg'
      GROUP BY t1.GRG_GHM, t1.Classe_poids_naiss_bis;
QUIT;