# ARS

Les [Agences Régionales de Santé](https://www.ars.sante.fr/) ont été créées  le 1 Avril 2010. 
Les objectifs de l’ARS vise à réduire les inégalités territoriales et sociales de santé (ISTS), améliorer l’espérance de vie en bonne santé et vise à garantir un système de santé de qualité, accessible et efficient sur tout le territoire. 

## Activité sur le SNDS

La connaissance de l’état de santé de la population régionale est un enjeu important pour élaborer une politique de santé publique. Observer pour comprendre, comprendre pour agir. 
Cette connaissance repose sur un travail partenarial que mène l’ARS depuis des années avec les ORS, Santé publique France en région (CIRE) et l’Assurance Maladie à partir de l’analyse et la production d’indicateurs sur l’état de santé de la population, sur ses déterminants démographiques, sociaux et environnementaux à l’échelle de différents territoires de la région.


## Mainteneurs du dossier

- Jérome Brocca <jerome.brocca@ars.sante.fr> 
    - Statisticien ARS Centre-Val de Loire
    - [LinkedIn](https://www.linkedin.com/in/j%C3%A9r%C3%B4me-brocca-73b61754)
- Céline Leroy <celine.leroy@ars.sante.fr>
    - Statisticienne ARS Normandie
    - [LinkedIn](https://www.linkedin.com/in/c%C3%A9line-leroy-b1408713a)
