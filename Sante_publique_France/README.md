# Santé publique France

[Santé publique France](https://documentation-snds.health-data-hub.fr/glossaire/SpF.html) est un établissement public administratif sous tutelle du ministère chargé de la Santé.  
Sa mission : améliorer et protéger la santé des populations.

## Mainteneurs du dossier

* Laurence Mandereau-Bruno : [Laurence.MANDEREAU-BRUNO@santepubliquefrance.fr](mailto:Laurence.MANDEREAU-BRUNO@santepubliquefrance.fr)
* Francis Chin : [Francis.CHIN@santepubliquefrance.fr](mailto:Francis.CHIN@santepubliquefrance.fr)
* Lisa Cahour : [Lisa.CAHOUR@santepubliquefrance.fr](mailto:Lisa.CAHOUR@santepubliquefrance.fr)
* Julie Chesneau : [Julie.CHESNEAU@santepubliquefrance.fr](mailto:Julie.CHESNEAU@santepubliquefrance.fr)

## Liste des programmes mis à disposition

### Programme "dcir_prestations_medicaments.sas"

Ce programme permet la sélection de prestations affinées en prenant comme exemple la table des médicaments.  
L'utilisateur choisit la liste des médicaments à sélectionner et la période d'intérêt.  
Il peut être adapté pour une autre table affinée (exemple : biologie, dispositifs médicaux...)

Détails et explications dans la fiche de documentation : 
[Requête type dans la table prestations du DCIR](https://documentation-snds.health-data-hub.fr/fiches/sas_prestation_dcir.html)


### Programme "dcir_infos_socio_eco_referentiels.sas"

Ce programme permet d'aller rechercher les informations socio-démographiques présentes dans les référentiels des bénéficiaires pour les individus d'une table déjà créée.  


Détails et explications dans la fiche de documentation : 
[Requête type dans la table prestations du DCIR](https://documentation-snds.health-data-hub.fr/fiches/sas_prestation_dcir.html)

### Programme "pmsi_mco_select_sejours_par_diag.sas"

Ce programme permet la sélection de séjours à partir de codes diagnostics (principal ou relié ou associés).  
Pour ces séjours, un certain nombre de variables décrivant le séjour sélectionné sont restituées en sortie. 


Détails et explications dans la fiche de documentation : 
[Requête type dans le PMSI-MCO](https://documentation-snds.health-data-hub.fr/fiches/requete_type_pmsi_mco.html)

### Programme "ald_select_par_code_cim.sas"

Ce programme permet la sélection d'ALD exonérante à partir des codes CIM-10.


Détails et explications dans la fiche de documentation : 
[Requête type de sélection des ALD](https://documentation-snds.health-data-hub.fr/fiches/requete_type_ald.html)
