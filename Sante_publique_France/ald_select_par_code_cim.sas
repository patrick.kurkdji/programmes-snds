/************************************************************************************/
/*   Copyright � 2020-2021 Sant� publique France                                    */
/*                                                                                  */
/*   Licensed under the Apache License, Version 2.0 (the "License");                */
/*   you may not use this file except in compliance with the License.               */
/*   You may obtain a copy of the License at                                        */
/*                                                                                  */
/*       http://www.apache.org/licenses/LICENSE-2.0                                 */
/*                                                                                  */
/*   Unless required by applicable law or agreed to in writing, software            */
/*   distributed under the License is distributed on an "AS IS" BASIS,              */
/*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.       */
/*   See the License for the specific language governing permissions and            */
/*   limitations under the License.                                                 */
/************************************************************************************/

/************************************************************************************/
/**** Param�tres � changer : r�pertoire, liste des codes CIM, p�riode            ****/
/************************************************************************************/

/*d�claration de la librairie */
%let fichier = %SYSGET (HOME)/sasdata;
libname ma_lib "&fichier/sasdata/ESP_PART/PGM_tmt";

/*** Exemple pour l�ALD hypertension art�rielle s�v�re en 2019 ***/
/********************************************************************/
/* LES ARGUMENTS A MODIFIER SONT LES DEUX MACRO�VARIABLES : LIST_CODES et AAAA */
/*liste de codes CIM � s�lectionner */
%let list_codes = I15 I10;

/* ann�e � consid�rer */
%let AAAA = 2019;

/************************************************************************************/
/*MACRO- VARIABLES QUI S�INITIALISENT AUTOMATIQUEMENT*/
/*date de d�but de la p�riode consid�r�e */
%let debut_ALD=01JAN&AAAA.;

/*date de fin de la p�riode consid�r�e*/
%let fin_ALD=31DEC&AAAA.;

/*date de point : date maximale d�insertion des exon�rations. 
Permet d�avoir un �tat des lieux des exon�rations � une date pr�cise et de toujours retrouver le m�me r�sultat � diff�rentes dates d�ex�cution du programme.
On estime que la quasi totalit� des exon�rations sont restitu�es 6 mois apr�s la fin de la p�riode d�int�r�t.
Le d�lai de 6 mois supprime '� tort' un tr�s faible nombre de p�riodes d'ALD*/
%let anpt = %eval(&AAAA + 1);
%let date_point=01JUL&anpt.;
%let nb_codes=%sysfunc(countw(&list_codes));

/************************************************************************************/
/************ PROGRAMME DE SELECTION DES ALD EXONERANTES                   **********/
/************************************************************************************/
/*S�lection des ALD exon�rantes par codes CIM dans le r�f�rentiel*/
%macro selection_ALD();
	/* S�lection des ALD exon�rantes en SQL code par code (+ rapide) - 1 table par code*/
	%do i=1 %to &nb_codes;
		%let code=%scan((&list_codes),&i);
		%put "&code.%";

		proc sql;
			create table ALD&i as 
			select * 
			from oravue.ir_imb_r 
			where med_mtf_cod like "&code.%" 
				and imb_etm_nat in (41,43,45) 
				and ins_dte<"&date_point.:0:0:0"dt;
		quit;

	%end;

	/*regroupement de toutes les tables*/
	data ALD;
		set %do i=1 %to &nb_codes;
				ALD&I
			%end; ;
	run;

	%do i = 1 %to &nb_codes;
		proc datasets library=work;
			delete ALD&i.;
		run;
	%end;
%mend;

%selection_ALD();

/*Tri pour ne garder que les lignes correspondant � la date d'insertion la plus r�cente pour chaque code et motif*/
/*Remarque :lorsque l�on ne s�int�resse qu�� une seule pathologie, il n�est pas n�cessaire de trier et de supprimer les doublons sur la variable med_mtf_cod */
proc sort data=ALD;
	by ben_nir_psa ben_rng_gem med_mtf_cod imb_etm_nat descending ins_dte descending upd_dte imb_ald_dtd imb_ald_dtf;
run;

proc sort data=ALD nodupkey;
	by ben_nir_psa ben_rng_gem med_mtf_cod imb_etm_nat;
run;

/*Restriction des ALD sur la p�riode d'int�r�t*/
data ALD_exo_12mois;
	set ALD;
	where imb_ald_dtd<="&fin_ALD.:0:0:0"dt and (imb_ald_dtf>="&debut_ALD.:0:0:0"dt or imb_ald_dtf=. or imb_ald_dtf='01JAN1600:0:0:0'dt);
run;